# Sprachfreund

> :warning: This project is in **active development** and might be incomplete, unstable, and generally rather unfinished. Run this locally at your own phyiscal and mental peril.

Sprachfreund is an interactive web page which let's you learn German or English by chatting with an AI. You can train specific scenarios like ordering at a restaurant or catching up with a friend, or just have a pleasant conversation without knowing where it will go. Sprachfreund will give you feedback on your grammar and style and thus make sure that you don't internalize any mistakes you might make. If you don't understand a word or a sentence the AI gives you, don't worry! You can just let Sprachfreund explain it to you. Practice and learn, all in one.

## Installation

If you want to try out the current state of the project locally, simply run the following steps:

### Dev Environment

To function properly, Sprachfreund needs to connect to a Redis instance, a Postgres instance, and a [LanguageTool server](https://dev.languagetool.org/http-server).
The easiest way to provide those is to use Docker. To create and run these Docker containers, execute these commands:

```bash
# Start Redis container
docker run --name dev-redis -p 6379:6379 -d redis

# Start Postgres container
docker run --name dev-postgres -p 5432:5432 -d postgres

# Start LanguageTool container
docker run --name dev-languagetool -p 8010:8010 erikvl87/languagetool
```

### API Keys

In addition to the instances in the last paragraph, Sprachfreund also needs access to the [OpenAI](https://openai.com/blog/openai-api) and [DeepL](https://www.deepl.com/pro-api?cta=header-pro-api) APIs.
Once you have generated API keys for these APIs, place them in a `.env` file within the `api` directory. The file should have the following format:

```
OPENAI_API_KEY=<OpenAI key>
DEEPL_API_KEY=<DeepL key>
```

### Run Project Locally

When the environment is setup like described above, the project can finally be run locally:

```bash
# Clone project
git clone https://gitlab.com/Buschium/sprachfreund.git

# Install Django dependencies
cd api
pip install -r requirements.txt

# Install Vue dependencies
cd ../app
pnpm i

# Run project locally
./start-dev.sh
```

## Roadmap

The main features I want to implement for this app are:

-   [x] Chat interface for chatting with the AI
    -   [x] Letting the user select their languages
    -   [x] Letting the user select their proficiency level
-   [x] Grammatical corrections for user messages
-   [x] Style corrections for user messages
-   [ ] Word and sentence explainer
-   [ ] Conversation scenarios
-   [ ] User management (save your progress)
-   [ ] Challenges and progression points
-   [ ] Vocabulary trainer

## License

This project is licensed unter the [GNU General Public License v3.0](https://www.gnu.org/licenses/gpl-3.0.en.html).
