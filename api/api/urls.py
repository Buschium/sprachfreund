"""
URL configuration for api project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from sprachfreund_chat.views import (
    get_mock_chat,
    get_chat,
    get_messages,
    get_session_token,
    create_chat,
    get_explanation,
    get_translation,
)
from sprachfreund_chat.consumers import ChatConsumer

urlpatterns = [
    path("admin/", admin.site.urls),
    path("token/", get_session_token, name="get_session_token"),
    path("api/get_mock_chat/", get_mock_chat),
    path("api/chat/<int:chat_id>/", get_chat, name="get_chat"),
    path("api/chat/<int:chat_id>/messages/", get_messages, name="get_messages"),
    path("api/create_chat/", create_chat, name="create_chat"),
    path("api/explanation/", get_explanation, name="get_explanation"),
    path("api/translation/", get_translation, name="get_translation"),
]

websocket_urlpatterns = [
    path("ws/chat/<int:chat_id>/", ChatConsumer.as_asgi()),
]
