from django.db import models


class User(models.Model):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)


class Chat(models.Model):
    creator_token = models.CharField(max_length=64)
    channel = models.CharField(max_length=100)
    draft_message = models.TextField(blank=True, default="")
    language = models.CharField(max_length=2)
    source_language = models.CharField(max_length=2)
    level = models.CharField(max_length=3)
    waiting_answer = models.BooleanField(default=False)
    waiting_corrections = models.BooleanField(default=False)
    waiting_explanation = models.BooleanField(default=False)
    has_answer_error = models.BooleanField(default=False)


class Message(models.Model):
    type = models.CharField(max_length=11, default="message")
    content = models.TextField()
    date = models.DateTimeField(auto_now_add=True)
    sender = models.ForeignKey(User, on_delete=models.CASCADE)
    has_corrections_error = models.BooleanField(default=False)
    chat = models.ForeignKey(Chat, related_name="messages", on_delete=models.CASCADE)


class PromptTemplate(models.Model):
    name = models.CharField(max_length=255)
    template = models.TextField()
    description = models.TextField(blank=True, default="")


class CorrectionTranslation(models.Model):
    rule_id = models.CharField(max_length=255)
    rule_subid = models.IntegerField(default=None, blank=True, null=True)
    language = models.CharField(max_length=2)
    translation = models.TextField()


class Correction(models.Model):
    rule_id = models.CharField(max_length=255)
    rule_subid = models.IntegerField(blank=True, null=True)
    offset = models.IntegerField()
    length = models.IntegerField()
    corrected_words = models.JSONField()
    category = models.CharField(max_length=255)
    message = models.ForeignKey(
        "Message", related_name="corrections", on_delete=models.CASCADE
    )


class Explanation(models.Model):
    type = models.CharField(max_length=11)
    offset = models.IntegerField()
    length = models.IntegerField()
    content = models.TextField()
    message = models.ForeignKey(
        "Message", related_name="explanations", on_delete=models.CASCADE
    )


class FailedTask(models.Model):
    type = models.CharField(max_length=11)
    task_id = models.UUIDField(unique=True)
    message = models.ForeignKey(
        "Message", related_name="failed_tasks", on_delete=models.CASCADE
    )
