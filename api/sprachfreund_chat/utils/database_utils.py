from channels.db import database_sync_to_async
from ..models import (
    FailedTask,
    Message,
    Chat,
    CorrectionTranslation,
    PromptTemplate,
    Correction,
    Explanation,
)
from urllib.parse import parse_qs


def write_message_to_db(message: dict, chat_id: int):
    """Takes a dictionary and tries to write it to the database as a Message object.

    Args:
        message (dict): Dictionary corresponding to a Message object
        chat_id (int): ID of the chat object to which the Message object belongs
    """
    created_message = Message.objects.create(
        type=message["type"],
        content=message["content"],
        date=message["date"],
        sender_id=message["sender"]["id"],
        chat_id=chat_id,
    )

    return created_message.id


def get_recent_messages(limit: int, chat_id: int):
    """Returns the <limit> most recent messages from a certain chat.

    Args:
        limit (int): The number of messages that should be retrieved
        chat_id (int): The ID of the chat for which the messages should be queried

    Returns:
        list(models.Message): A list of the queried Message objects
    """
    messages_list = list(
        Message.objects.select_related("sender")
        .prefetch_related("corrections", "explanations")
        .filter(chat_id=chat_id)
        .order_by("-date")[:limit]
    )
    messages_list.reverse()
    return messages_list


@database_sync_to_async
def get_chat(chat_id: int):
    """Returns the Chat object corresponding to an ID.

    Args:
        chat_id (int): ID of the Chat object to be queried

    Returns:
        models.Chat: Chat object corresponding to the ID
    """
    try:
        return Chat.objects.get(pk=chat_id)
    except Chat.DoesNotExist:
        return None


@database_sync_to_async
def validate_session_token(scope, chat_id: int):
    """Checks whether a session token authorizes to access a certain chat.

    Args:
        scope (any): The Django session scope which contains the session token
        chat_id (int): The ID of the chat against which the token is checked

    Returns:
        boolean: Whether or not the session token authorizes to access the chat
    """
    query_string = scope["query_string"].decode("utf-8")
    query_params = parse_qs(query_string)
    token = query_params.get("token", [None])[0]

    try:
        chat = Chat.objects.get(pk=chat_id)
        return (
            token is not None
            and chat.creator_token == token
            or chat.creator_token == "0" * 64  # Allow connection to debug chat
        )
    except Chat.DoesNotExist:
        return False


def get_translation_for_correction(correction: Correction):
    """Queries the translation of a correction object's body from the database.

    Args:
        correction (models.Correction): The correction object

    Returns:
        string: The translated correction body with the corrected words inserted
    """
    try:
        message = correction.message
        chat = message.chat
        source_language = chat.source_language
        correction_translation = CorrectionTranslation.objects.get(
            rule_id=correction.rule_id,
            rule_subid=correction.rule_subid,
            language=source_language,
        )
        return correction_translation.translation.format(*correction.corrected_words)
    except CorrectionTranslation.DoesNotExist:
        return ""
    except Exception as e:
        print(e)


def get_chat_prompt_template():
    """Returns the prompt template for chats.

    Returns:
       string: The prompt template
    """
    return PromptTemplate.objects.get(name="chat_template").template


def get_explanation_prompt_template():
    """Returns the prompt template for explanations.

    Returns:
       string: The prompt template
    """
    return PromptTemplate.objects.get(name="explanation_template").template


def persist_correction(correction: Correction, message_id: int):
    """Writes a correction object to the database.

    Args:
        correction (models.Correction): The correction to be written
        message_id (int): The ID of the message to which the correction object
        should be associated
    """
    Correction.objects.create(
        rule_id=correction["rule_id"],
        rule_subid=correction["rule_subid"],
        offset=correction["offset"],
        length=correction["length"],
        corrected_words=correction["corrected_words"],
        category=correction["category"],
        message_id=message_id,
    )


def get_persisted_translation(rule_id: str, rule_subid: int, language: str):
    """Gets the translation for a certain rule in a certain language.

    Args:
        rule_id (str): The LanguageTool rule ID
        rule_subid (int): The LanguageTool rule sub-ID
        language (str): The ISO 639-1 of the language of the translation

    Returns:
        str: The translation, if found
    """
    try:
        correction = CorrectionTranslation.objects.get(
            rule_id=rule_id, rule_subid=rule_subid, language=language
        )
        return correction.translation
    except CorrectionTranslation.DoesNotExist:
        return None


def persist_translation(rule_id: str, rule_subid: int, language: str, translation: str):
    """Writes a CorrectionTranslation object to the database.

    Args:
        rule_id (str): The LanguageTool rule ID
        rule_subid (int): The LanguageTool rule sub-ID
        language (str): The ISO 639-1 of the language of the translation
        translation (str): The actual translation
    """
    CorrectionTranslation.objects.create(
        rule_id=rule_id,
        rule_subid=rule_subid,
        language=language,
        translation=translation,
    )


def persist_explanation(
    message_id: int, type: str, offset: int, length: int, content: str
):
    """Writes an Explanation object to the database.

    Args:
        message_id (int): The ID of the message to which this explanation belongs
        type (str): The type of the explanation (translation or explanation)
        offset (int): The offset of the explained passage within the message
        length (int): The length of the explained passage
        content (str): The actual explanation
    """
    Explanation.objects.create(
        type=type, offset=offset, length=length, content=content, message_id=message_id
    )


def remove_message(message_id: int):
    """Removes a single message from the database (if it exists)

    Args:
        message_id (int): The ID of the message to be removed
    """
    Message.objects.filter(id=message_id).delete()


@database_sync_to_async
def get_failed_task(message_id: int):
    """Returns a failed task associated with a message

    Args:
        message_id (int): The ID of the message

    Returns:
        FailedTask: The failed task
    """
    try:
        failed_task = FailedTask.objects.get(message_id=message_id)
        return failed_task
    except FailedTask.DoesNotExist:
        return None


@database_sync_to_async
def get_latest_user_message(chat_id: int):
    """Gets the latest message sent by the user in a chat.

    Args:
        chat_id (int): The ID of the chat

    Returns:
        Message: The latest message sent by the user
    """
    try:
        message = Message.objects.filter(sender_id=1, chat_id=chat_id).latest("date")
        return message
    except Message.DoesNotExist:
        return None


@database_sync_to_async
def get_latest_message(chat_id: int):
    """Gets the latest message sent by either the server or the user in a chat.

    Args:
        chat_id (int): The ID of the chat

    Returns:
        Message: The latest message sent to the chat
    """
    try:
        message = Message.objects.filter(chat_id=chat_id).latest("date")
        return message
    except Message.DoesNotExist:
        return None


def is_waiting_explanation(chat_id: int):
    """Returns whether or not a chat is waiting for an explanation/translation

    Args:
        chat_id (int): The ID of the chat

    Returns:
        boolean: Whether the chat is waiting for an explanation/translation
    """
    try:
        return Chat.objects.get(pk=chat_id).waiting_explanation
    except Chat.DoesNotExist:
        return False


@database_sync_to_async
def set_channel(chat_id: int, channel: str):
    """Sets the channel name for a chat

    Args:
        chat_id (int): The ID of the chat for which the channel name is to be set
        channel (str): The channel name
    """
    Chat.objects.filter(pk=chat_id).update(channel=channel)


def get_channel(chat_id: int):
    """Gets the channel name for a chet

    Args:
        chat_id (int): The ID of the chat

    Returns:
        str: The channel name
    """
    try:
        return Chat.objects.get(pk=chat_id).channel
    except Chat.DoesNotExist:
        return ""
