import deepl
import requests
from django.conf import settings

translator = deepl.Translator(settings.DEEPL_API_KEY)


def translate_text(text: str, target_lang: str):
    return translate_text_deepl(text, target_lang)


def translate_text_deepl(text: str, target_lang: str):
    target_lang = "en-US" if target_lang == "en" else target_lang
    return translator.translate_text(
        text=text, target_lang=target_lang, preserve_formatting=True
    ).text


def translate_text_itranslate(text: str, target_lang: str):
    url = "https://dev-api.itranslate.com/translation/v2/"
    headers = {
        "Authorization": f"Bearer {settings.ITRANSLATE_API_KEY}",
        "Content-Type": "application/json",
    }

    data = {
        "source": {"dialect": "auto", "text": text},
        "target": {"dialect": target_lang},
    }

    response = requests.post(url, json=data, headers=headers)

    if response.status_code == 200:
        return response.json()["target"]["text"]
    else:
        return None
