from rest_framework import permissions

class HasSessionToken(permissions.BasePermission):
    """
    Custom permission to only allow access if there's a session token.
    """

    def has_permission(self, request, view):
        return 'session_token' in request.session
