import json
import asyncio
from channels.generic.websocket import AsyncWebsocketConsumer
from .utils.database_utils import (
    get_chat,
    validate_session_token,
    get_failed_task,
    get_latest_user_message,
    get_latest_message,
    set_channel,
)
from .tasks import (
    get_answer_prompt,
    get_languagetool_corrections,
    get_openai_response,
    serialize_recent_messages,
    set_waiting_answer,
    set_waiting_corrections,
    translate_corrections,
    create_response_message,
    jsonify_corrections,
    create_correction_message,
    send_message_to_ws,
    dmap,
    write_message,
    send_placeholder_message,
    handle_message_error,
    handle_answer_error,
    handle_corrections_error,
)
from celery import chain, group


DATE_FORMAT = "%Y-%m-%dT%H:%M:%S"


class ChatConsumer(AsyncWebsocketConsumer):
    chat_prompt_template = None
    chat = None

    async def connect(self):
        chat_id = self.scope["url_route"]["kwargs"]["chat_id"]
        self.room_group_name = f"chat_{chat_id}"

        chat, is_token_valid = await asyncio.gather(
            get_chat(chat_id), validate_session_token(self.scope, chat_id)
        )

        self.chat = chat

        if not is_token_valid:
            await self.close(code=1008)
            return

        # Join the chat room
        await self.channel_layer.group_add(self.room_group_name, self.channel_name)
        await set_channel(chat_id, self.channel_name)
        await self.accept()

        latest_message = await get_latest_message(self.chat.id)
        if latest_message:
            # Fetch recent messages from database and send to the user
            chain(
                serialize_recent_messages.s(20, self.chat.id),
                dmap.s(send_message_to_ws.s(self.chat.id)),
            ).apply_async()
        else:
            # Have Sprachfreund initiate the conversation
            init_chain = chain(
                set_waiting_answer.si(self.chat.id, True),
                send_placeholder_message.si({}, self.chat.id),
            )
            apply_chain = init_chain.apply_async()
            placeholder_message_id = apply_chain.get()

            answer_chain = chain(
                set_waiting_answer.si(self.chat.id, True),
                get_answer_prompt.si(self.chat.language, self.chat.level),
                get_openai_response.s("Hi!"),
                create_response_message.s(placeholder_message_id, self.chat.id),
                send_message_to_ws.s(self.chat.id),
                set_waiting_answer.si(self.chat.id, False),
            )
            answer_chain.apply_async(
                ignore_result=True,
                link_error=[
                    handle_answer_error.s(placeholder_message_id, self.chat.id),
                    set_waiting_answer.si(self.chat.id, False),
                ],
            )

    async def disconnect(self, close_code):
        # Leave the chat room
        await self.channel_layer.group_discard(self.room_group_name, self.channel_name)

    async def receive(self, text_data):
        data = json.loads(text_data)
        if data["type"] == "message":
            await self.handle_message(data)
        elif data["type"] == "retry":
            await self.handle_retry_request(data)

    async def handle_message(self, message_data):
        # Check whether the last message in chat was from server
        latest_message = await get_latest_message(self.chat.id)
        if latest_message and latest_message.sender_id == 1:
            print(
                f"Latest message in chat {self.chat.id} was from user, rejecting message"
            )
            await self.send(
                text_data=json.dumps(
                    {
                        "type": "message_error",
                        "content": "Latest message was from user",
                    }
                )
            )
            return

        # Write user message to database and send it to Websocket
        user_message = None
        placeholder_message_id = None
        try:
            init_chain = chain(
                write_message.s(message_data, self.chat.id),
                send_message_to_ws.s(self.chat.id),
                send_placeholder_message.s(self.chat.id),
            )
            apply_chain = init_chain.apply_async(
                link_error=handle_message_error.s(self.chat.id)
            )
            user_message, placeholder_message_id = apply_chain.get()
        except Exception as e:
            print(f"Error occurred: {e}")
            return

        await self.start_answer_chain(user_message["content"], placeholder_message_id)

        # Get corrections for user message
        corrections_chain = chain(
            set_waiting_corrections.si(self.chat.id, True),
            get_languagetool_corrections.si(
                self.chat.language, user_message["content"], user_message["id"]
            ),
            translate_corrections.s(self.chat.source_language),
            jsonify_corrections.s(),
            create_correction_message.s(user_message["id"]),
            send_message_to_ws.s(self.chat.id),
            set_waiting_corrections.si(self.chat.id, False),
        )
        corrections_chain.apply_async(
            ignore_result=True,
            link_error=[
                handle_corrections_error.s(user_message["id"], self.chat.id),
                set_waiting_corrections.si(self.chat.id, False),
            ],
        )

    async def handle_retry_request(self, data):
        failed_task = await get_failed_task(data["id"])
        user_message = await get_latest_user_message(self.chat.id)
        if failed_task and failed_task.type == "answer":
            await self.start_answer_chain(user_message.content, data["id"])
        else:
            await self.send(
                text_data=json.dumps(
                    {
                        "type": "retry_error",
                        "content": f"Message with ID {data['id']} has no associated failed task of type 'answer'",
                    }
                )
            )

    async def start_answer_chain(self, content, placeholder_message_id):
        # Get answer for user message
        answer_chain = chain(
            set_waiting_answer.si(self.chat.id, True),
            group(
                serialize_recent_messages.si(20, self.chat.id),
                get_answer_prompt.si(self.chat.language, self.chat.level),
            ),
            get_openai_response.s(content),
            create_response_message.s(placeholder_message_id, self.chat.id),
            send_message_to_ws.s(self.chat.id),
            set_waiting_answer.si(self.chat.id, False),
        )
        answer_chain.apply_async(
            ignore_result=True,
            link_error=[
                handle_answer_error.s(placeholder_message_id, self.chat.id),
                set_waiting_answer.si(self.chat.id, False),
            ],
        )

    async def message_send(self, event):
        message_data = event["message"]
        await self.send(text_data=json.dumps(message_data))
