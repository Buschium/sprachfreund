from django.apps import AppConfig


class SprachfreundChatConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "sprachfreund_chat"
