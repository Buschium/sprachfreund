from rest_framework import serializers
from .models import Chat, Explanation, Message, User, Correction


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["id", "first_name", "last_name"]


class ChatSerializer(serializers.ModelSerializer):
    waitingForAnswer = serializers.BooleanField(source="waiting_answer", required=False)
    waitingForExplanation = serializers.BooleanField(source="waiting_explanation", required=False)
    hasAnswerError = serializers.BooleanField(source="has_answer_error", required=False)

    class Meta:
        model = Chat
        fields = [
            "id",
            "creator_token",
            "draft_message",
            "language",
            "source_language",
            "level",
            "waitingForAnswer",
            "waitingForExplanation",
            "hasAnswerError",
        ]

    def validate_language(self, value):
        SUPPORTED_LANGUAGES = ["en", "de"]
        if value not in SUPPORTED_LANGUAGES:
            raise serializers.ValidationError("Language not supported.")
        return value

    def validate_level(self, value):
        CEFR_LEVELS = ["A1", "A2", "B1", "B2", "C1", "C2"]
        if value not in CEFR_LEVELS:
            raise serializers.ValidationError("Invalid CEFR level.")
        return value


class CorrectionSerializer(serializers.ModelSerializer):
    body = serializers.CharField(source="translated_message", read_only=True)

    class Meta:
        model = Correction
        fields = [
            "rule_id",
            "rule_subid",
            "offset",
            "length",
            "corrected_words",
            "category",
            "body",
        ]


class ExplanationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Explanation
        fields = ["type", "offset", "length", "content", "message_id"]


class MessageSerializer(serializers.ModelSerializer):
    sender = UserSerializer()
    corrections = CorrectionSerializer(many=True, read_only=True)
    explanations = ExplanationSerializer(many=True, read_only=True)
    hasCorrectionsError = serializers.BooleanField(source="has_corrections_error")

    class Meta:
        model = Message
        fields = [
            "id",
            "type",
            "content",
            "date",
            "sender",
            "hasCorrectionsError",
            "corrections",
            "explanations",
        ]
