from celery import shared_task
from .utils.database_utils import (
    get_chat_prompt_template,
    get_explanation_prompt_template,
    get_recent_messages,
    persist_correction,
    persist_explanation,
    get_persisted_translation,
    persist_translation,
    write_message_to_db,
    get_translation_for_correction,
    remove_message,
    get_channel,
)
from .utils.translation_utils import translate_text
from .serializers import MessageSerializer
from .models import Chat, Message, FailedTask
from pylanguagetool import api
from django.conf import settings
from datetime import datetime
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync
from celery import subtask, group
from celery.utils.log import get_task_logger
import json
import re
from openai import OpenAI

client = OpenAI(api_key=settings.OPENAI_API_KEY)

logger = get_task_logger(__name__)



MODEL = settings.MODEL

LANGUAGETOOL_URL = settings.LANGUAGETOOL_URL

DATE_FORMAT = "%Y-%m-%dT%H:%M:%S"

LANGUAGES = {
    "ar": "Arabic",
    "de": "German",
    "en": "English",
    "es": "Spanish",
    "fr": "French",
    "ru": "Russian",
    "tr": "Turkish",
    "uk": "Ukrainian",
}

LEVELS = {
    "A1": "beginner",
    "A2": "elementary",
    "B1": "intermediate",
    "B2": "upper intermediate",
    "C1": "advanced",
    "C2": "proficient",
}

LEVEL_CAPABILITIES = {
    "A1": "- Very basic, everyday phrases\n- Very short, simple texts, familiar names and words\n- Simple sentences in present tense and indicative",
    "A2": "- Very basic personal, family and job-related language\n- Short, simple texts on familiar matter\n.- Simple sentences in present and past tense",
    "B1": "- Common topics at work, school, or traveling\n- Factual texts on the user's subjects of interest\n- Common sentences in past, present, and future",
    "B2": "- More complex text\n- Texts with a broad reading vocabulary and large degree of autonomy\n- More complex grammatical constructions like subjunctives",
    "C1": "- Lengthy text on abstract topics\n- Complex text with details not relating to everyday life",
    "C2": "- Abstract, structurally complex text and literary style",
}

GERMAN_EXAMPLES = {
    "A1": "Ich heiße Johann. Wie geht es dir?",
    "A2": "Ich habe zwei Katzen und sie sind sehr verspielt.",
    "B1": "Ich bin gestern ins Kino gegangen und habe einen Film gesehen.",
    "B2": "Trotz des Regens haben wir uns entschieden, wandern zu gehen.",
    "C1": "Der Klimawandel stellt eine erhebliche Bedrohung für globale Ökosysteme dar.",
    "C2": "Philosophische Diskussionen gehen oft auf abstrakte Konzepte ein, die das konventionelle Denken herausfordern.",
}

ENGLISH_EXAMPLES = {
    "A1": "I am John. How are you?",
    "A2": "I have two cats and they are very playful.",
    "B1": "I went to the cinema yesterday and watched a movie.",
    "B2": "Despite the rain, we decided to go hiking.",
    "C1": "Climate change poses a significant threat to global ecosystems.",
    "C2": "Philosophical discussions often delve into abstract concepts that challenge conventional wisdom.",
}


@shared_task(soft_time_limit=10, time_limit=20)
def get_answer_prompt(language: str, level: str):
    """Reads the LLM prompt for getting an answer to a user message from the database

    Args:
        language (str): The language in which the answer should be
        level (str): The language difficulty level at which the answer should be

    Returns:
        str: The complete formatted prompt
    """
    prompt_template = get_chat_prompt_template()
    language = "German" if language == "de" else "English"
    informal_level = LEVELS[level]
    capabilities = LEVEL_CAPABILITIES[level]
    example = GERMAN_EXAMPLES[level] if language == "de" else ENGLISH_EXAMPLES[level]
    role = "conversational partner for language practice"

    formatted_prompt = prompt_template.format(
        language=language,
        level=informal_level,
        role=role,
        capabilities=capabilities,
        example=example,
    )

    return formatted_prompt


@shared_task(soft_time_limit=10, time_limit=20)
def get_explanation_prompt(context: str, language: str, source_language: str):
    """Reads the LLM prompt for getting an explanation for a message fragment
    from the database

    Args:
        context (str): The entire message in which the fragment to explain is contained
        language (str): The language in which the message is
        source_language (str): The language in which the explanation should be

    Returns:
        str: An explanation of the message fragment in sourcec_language
    """
    prompt_template = get_explanation_prompt_template()
    formatted_prompt = prompt_template.format(
        LANGUAGES[language],
        context,
        LANGUAGES[source_language],
    )
    return formatted_prompt


@shared_task(soft_time_limit=15, time_limit=30)
def get_languagetool_corrections(
    language: str, user_message: str, user_message_id: int
):
    """Gets corrections for a message from the LanguageTool API

    Args:
        language (str): The language in which the message is
        user_message (str): The message for which the corrections should be created
        user_message_id (int): The database ID of the message

    Raises:
        ValueError: If the message sent by the user doesn't fit the language set for the chat

    Returns:
        list: List of all corrections returned by LanguageTool
    """
    chat_language = "de-DE" if language == "de" else "en-US"
    response = api.check(
        user_message,
        api_url=LANGUAGETOOL_URL,
        lang=chat_language,
        disabled_categories="CORRESPONDENCE,GENDER_NEUTRALITY,CREATIVE_WRITING,WIKIPEDIA",
    )
    if (
        len(user_message) >= 10
        and response["language"]["detectedLanguage"]["code"] != chat_language
        or response["language"]["detectedLanguage"]["confidence"] <= 0.5
    ):
        raise ValueError("User message doesn't fit chat language.")

    corrections = []
    for match in response["matches"]:
        body = match["rule"]["category"]["name"] + "|" + match["message"]
        correction = {
            "offset": match["offset"],
            "length": match["length"],
            "rule_id": match["rule"]["id"],
            "rule_subid": match["rule"]["subId"] if "subId" in match["rule"] else None,
            "category": match["rule"]["category"]["id"],
            "corrected_words": get_corrected_words(body),
            "body": get_translation_template(body),
        }
        persist_correction(correction, user_message_id)
        corrections.append(correction)
    return corrections


def get_corrected_words(message: str):
    """Extracts all words that have been corrected from a LanguageTool correction

    Args:
        message (str): The correction message from LanguageTool

    Returns:
        list: All words that have been corrected
    """
    return re.findall(r"[„“‚‘](.*?)[„“‚‘]", message)


def get_translation_template(message: str):
    """Returns the LanguageTool correction message without the corrected words.

    Args:
        message (str): The correction message from LanguageTool

    Returns:
        str: The message without the corrected words
    """
    return re.sub(r"[„“‚‘].*?[„“‚‘]", '"{}"', message)


@shared_task(soft_time_limit=30, time_limit=60)
def get_openai_response(chain_data, user_message: str):
    """Gets the response for a prompt from the OpenAI API

    Args:
        chain_data (list or str): Either a tuple of a list of past messages and a
                                  prompt text, or just the prompt text
        user_message (str): The user message for which an answer should be created

    Returns:
        str: The response from the OpenAI API
    """
    messages = None
    prompt_text = None
    if isinstance(chain_data, str):
        prompt_text = chain_data
    elif isinstance(chain_data, list):
        messages, prompt_text = chain_data
    old_messages = (
        [
            build_old_message(message)
            for message in messages
            if message["type"] != "placeholder"
        ]
        if messages
        else []
    )
    new_messages = [
        {"role": "system", "content": prompt_text},
        {"role": "user", "content": user_message},
    ]
    response = client.chat.completions.create(model=MODEL,
    messages=old_messages + new_messages,
    temperature=0)
    return response.choices[0].message.content


def build_old_message(message: dict):
    """Transforms a past message from the database into the format needed by OpenAI

    Args:
        message (dict): The old message

    Returns:
        obj: The formatted message
    """
    return {
        "role": "assistant" if message["sender"]["id"] == 0 else "user",
        "content": message["content"],
    }


@shared_task
def persist_explanation_task(
    response: str,
    context: str,
    user_message: str,
    user_message_id: int,
    explanation_type: str,
):
    """Writes an explanation to the database

    Args:
        response (str): The explanation as returned as an LLM response
        context (str): The entire message for which the explanation was genereated
        user_message (str): The message fragment that is explained
        user_message_id (int): The is of the message that is explained
        explanation_type (str): The type of explanation (translation or explanation)

    Returns:
        str: The explanation for further processing
    """
    offset = context.find(user_message)
    length = len(user_message)
    persist_explanation(
        user_message_id,
        explanation_type,
        offset,
        length,
        response,
    )
    return response


@shared_task(soft_time_limit=20, time_limit=40)
def translate_corrections(corrections: list, language: str):
    """Translates a list of corrections

    Args:
        corrections (list): A list of corrections as returned by LanguageTool
        language (str): The language into which the corrections should be translated

    Returns:
        list: A list of translated corrections
    """
    translated_corrections = (
        [get_translation(language, correction) for correction in corrections]
        if corrections
        else []
    )
    return translated_corrections


def get_translation(language: str, correction: dict):
    """Translates a single correction

    Args:
        language (str): The language into which the correction should be translated
        correction (dict): The correction to translate

    Returns:
        str: The translated correction
    """
    translation = get_persisted_translation(
        correction["rule_id"], correction["rule_subid"], language
    )
    if not translation:
        translation = translate_text(
            text=correction["body"],
            target_lang=language,
        )
        persist_translation(
            correction["rule_id"], correction["rule_subid"], language, translation
        )
    correction["body"] = translation.format(*correction["corrected_words"])
    return correction


@shared_task
def create_response_message(
    response_data: str, placeholder_message_id: int, chat_id: int
):
    """Creates a response message to send to the client in the right format

    Args:
        response_data (str): The answer from the LLM API
        chat_id (int): The database ID of the chat to which the message is going to be sent

    Returns:
        dict: The message object
    """
    current_date = datetime.now().strftime(DATE_FORMAT)
    Message.objects.filter(pk=placeholder_message_id).update(
        type="message", date=current_date, content=response_data
    )

    response = {
        "id": placeholder_message_id,
        "type": "patch",
        "content": response_data,
        "date": current_date,
        "sender": {"id": 0, "firstName": "Sprach", "lastName": "Freund"},
    }
    return response


@shared_task
def jsonify_corrections(corrections):
    return json.dumps(corrections)


@shared_task
def create_correction_message(corrections_json: str, user_message_id: int):
    """Creates a correction message to send to the client in the right format

    Args:
        corrections_json (str): The corrections array, in JSON format
        user_message_id (int): The database ID of the message which is corrected

    Returns:
        dict: The message object
    """
    return {
        "id": user_message_id,
        "type": "correction",
        "content": corrections_json,
        "date": datetime.now().strftime(DATE_FORMAT),
        "sender": {"id": 0, "firstName": "Sprach", "lastName": "Freund"},
    }


@shared_task
def create_explanation_message(explanation_json: str, offset: int, length: int):
    """Creates an explanation message to send to the client in the right format

    Args:
        explanation_json (str): The explanation data in JSON format
        offset (int): The offset of the explained fragment within the user message
        length (int): The length of the explained fragment

    Returns:
        dict: The message object
    """
    return {
        "type": "explanation",
        "offset": offset,
        "length": length,
        "content": explanation_json,
    }


@shared_task
def send_message_to_ws(message_data: dict, chat_id: int):
    """Sends a single message to the client over Websocket

    Args:
        message_data (dict): The message object
        chat_id (int): The ID of the chat to which the message should be sent

    Returns:
        dict: The message object, for further processing
    """
    channel_layer = get_channel_layer()
    channel_name = get_channel(chat_id)
    send_sync = async_to_sync(channel_layer.send)
    send_sync(channel_name, {"type": "message.send", "message": message_data})
    return message_data


@shared_task
def serialize_recent_messages(limit: int, chat_id: int):
    """Gets the most recent messages for a chat from the database

    Args:
        limit (int): The number of messages that should be retrieved
        chat_id (int): The ID of the chat for which the messages should be retrieved

    Returns:
        list: A list of recent messages
    """
    recent_messages = get_recent_messages(limit, chat_id)
    return [serialize_message(message) for message in recent_messages]


def serialize_message(message: Message):
    """Serialized a message to JSON format

    Args:
        message (Message): The message to be serialized

    Returns:
        str: The message, in JSON format
    """
    for correction in message.corrections.all():
        correction.translated_message = get_translation_for_correction(correction)

    serializer = MessageSerializer(message)
    serialized_data = serializer.data
    return serialized_data


@shared_task
def dmap(it, callback):
    """Creates a Celery task for each element in a list of arguments

    Args:
        it (list): The list of arguments
        callback (function): The function to be called

    Returns:
        A Celery group of all the created tasks
    """
    callback = subtask(callback)
    return group(
        callback.clone(
            [
                arg,
            ]
        )
        for arg in it
    )()


@shared_task
def send_placeholder_message(message_data: dict, chat_id: int):
    """Sends a placeholder message to the client

    Args:
        message_data (dict): The user message received from the client
        chat_id (int): The chat to which the placeholder message should be sent

    Returns:
        dict: The message object that was sent
    """
    placeholder_message = {
        "type": "placeholder",
        "date": datetime.now().strftime(DATE_FORMAT),
        "content": "",
        "sender": {
            "id": 0,
            "firstName": "Sprach",
            "lastName": "Freund",
        },
    }
    placeholder_message["id"] = write_message_to_db(placeholder_message, chat_id)
    channel_layer = get_channel_layer()
    channel_name = get_channel(chat_id)
    send_sync = async_to_sync(channel_layer.send)
    send_sync(
        channel_name,
        {
            "type": "message.send",
            "message": placeholder_message,
        },
    )
    if message_data:
        return [message_data, placeholder_message["id"]]
    else:
        return placeholder_message["id"]


@shared_task
def write_message(message_data: dict, chat_id: int):
    """Writes a single message to the database

    Args:
        message_data (dict): The message to be written
        chat_id (int): The ID of the chat to which the message was sent

    Returns:
        dict: The message data, for further processing
    """
    message_data["id"] = write_message_to_db(message_data, chat_id)
    return message_data


@shared_task
def handle_message_error(request, error, traceback, chat_id: int):
    """Handles an error in the message chain"""
    logger.warn(
        "Task {0} of Message Chain raised exception: {1!r}\n{2!r}".format(
            request.id, error, traceback
        )
    )
    remove_message(request.args[0]["id"])
    channel_name = get_channel(chat_id)
    send_error_message_to_ws(
        {"type": "message_error", "content": str(error)}, channel_name
    )


@shared_task
def handle_answer_error(request, error, traceback, message_id, chat_id: int):
    """Handles an error in the answer chain"""
    logger.warn(
        "Task {0} of Answer Chain raised exception: {1!r}\n{2!r}".format(
            request.id, error, traceback
        )
    )
    FailedTask.objects.create(
        type="answer", task_id=request.root_id, message_id=message_id
    )
    Chat.objects.filter(pk=chat_id).update(has_answer_error=True)
    channel_name = get_channel(chat_id)
    send_error_message_to_ws(
        {"type": "answer_error", "content": str(error)}, channel_name
    )


@shared_task
def handle_corrections_error(request, error, traceback, message_id, chat_id):
    """Handles an error in the corrections chain"""
    logger.warn(
        "Task {0} of Corrections Chain raised exception: {1!r}\n{2!r}".format(
            request.id, error, traceback
        )
    )
    set_message_errored(message_id)
    channel_name = get_channel(chat_id)
    send_error_message_to_ws(
        {"type": "corrections_error", "id": message_id, "content": str(error)},
        channel_name,
    )


def send_error_message_to_ws(message: dict, chat_id: int):
    """Sends an error message to a Websocket channel

    Args:
        message (dict): The message to be sent
        chat_id (int): The ID of the chat to which the message is to be sent
    """
    channel_layer = get_channel_layer()
    channel_name = get_channel(chat_id)
    send_sync = async_to_sync(channel_layer.send)
    send_sync(
        channel_name,
        {"type": "message.send", "message": message},
    )


def set_chat_errored(chat_id: int):
    Chat.objects.filter(pk=chat_id).update(has_answer_error=True)
    return None


def set_message_errored(message_id: int):
    Message.objects.filter(pk=message_id).update(has_corrections_error=True)
    return None


@shared_task
def set_waiting_answer(chat_id: int, waiting: bool):
    if waiting:
        Chat.objects.filter(pk=chat_id).update(
            waiting_answer=waiting, has_answer_error=False
        )
    else:
        Chat.objects.filter(pk=chat_id).update(waiting_answer=waiting)
    return None


@shared_task
def set_waiting_corrections(chat_id: int, waiting: bool):
    Chat.objects.filter(pk=chat_id).update(waiting_corrections=waiting)
    return None


@shared_task
def set_waiting_explanation(chat_id: int, waiting: bool):
    Chat.objects.filter(pk=chat_id).update(waiting_explanation=waiting)
    return None
