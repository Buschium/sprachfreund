from django.contrib import admin
from .models import PromptTemplate


# Register your models here.
@admin.register(PromptTemplate)
class PromptTemplateAdmin(admin.ModelAdmin):
    pass
