# Django imports
from django.http import JsonResponse, HttpResponse
from django.shortcuts import get_object_or_404

# Rest framework imports
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework import status

# Module imports
from .models import Chat, Explanation, Message
from .serializers import ChatSerializer
from .permissions import HasSessionToken
from .utils.translation_utils import translate_text
from .tasks import (
    get_explanation_prompt,
    get_openai_response,
    persist_explanation_task,
    set_waiting_explanation,
    create_explanation_message,
)
from .utils.database_utils import is_waiting_explanation

# Other imports
from celery import chain
import json
import secrets


def get_mock_chat(request):
    with open("sprachfreund_chat/mock/mock_chat.json", "r") as file:
        data = json.load(file)
    return JsonResponse(data)


@api_view(["GET"])
def get_chat(request, chat_id):
    chat = get_object_or_404(Chat, pk=chat_id)
    serializer = ChatSerializer(chat)
    return Response(serializer.data)


@api_view(["GET"])
def get_messages(request, chat_id):
    try:
        limit = int(request.GET.get("limit", 20))
        offset = int(request.GET.get("offset", 0))

        messages = (
            Message.objects.select_related("sender")
            .filter(chat_id=chat_id)
            .order_by("-date")[offset : offset + limit]
        )

        data = [
            {
                "id": message.id,
                "content": message.content,
                "date": message.date,
                "sender": {
                    "id": message.sender.id,
                    "firstName": message.sender.first_name,
                    "lastName": message.sender.last_name,
                },
            }
            for message in messages
        ]
        data.reverse()

        return JsonResponse(data, safe=False)
    except Exception as e:
        return JsonResponse(
            {"error": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR
        )


@api_view(["GET"])
def get_session_token(request):
    # Check if a session token already exists
    if "session_token" not in request.session:
        request.session["session_token"] = secrets.token_hex(32)
        request.session.modified = True

    return JsonResponse({"session_token": request.session["session_token"]})


@api_view(["POST"])
@permission_classes([HasSessionToken])
def create_chat(request):
    language = request.data.get("language")
    source_language = request.data.get("source_language")
    level = request.data.get("level")
    session_token = request.session.get("session_token")

    if not all([language, source_language, level, session_token]):
        missing_fields = [
            field
            for field in ["language", "source_language", "level", "session_token"]
            if not locals()[field]
        ]
        return JsonResponse(
            {"detail": f"Missing fields: {', '.join(missing_fields)}."},
            status=status.HTTP_400_BAD_REQUEST,
        )

    data = request.data
    data["creator_token"] = session_token
    data["messages"] = []

    serializer = ChatSerializer(data=data)

    if serializer.is_valid():
        serializer.save()
        return JsonResponse(serializer.data, status=status.HTTP_201_CREATED)
    else:
        return JsonResponse(
            serializer.errors, status=status.HTTP_500_INTERNAL_SERVER_ERROR
        )


@api_view(["GET"])
def get_explanation(request):
    message_id = int(request.GET.get("message_id"))
    offset = int(request.GET.get("offset"))
    length = int(request.GET.get("length"))
    language = request.GET.get("language")
    source_language = request.GET.get("source_language")

    if None in [message_id, offset, length, language, source_language]:
        return HttpResponse(
            "All required fields must be provided.", status=status.HTTP_400_BAD_REQUEST
        )

    message = get_object_or_404(Message, pk=message_id)
    if is_waiting_explanation(message.chat_id):
        return HttpResponse(
            "An explanation is already in process. Try again after the current explanation finishes.",
            status=status.HTTP_429_TOO_MANY_REQUESTS,
        )

    try:
        context = message.content
        explanation_content = context[offset : offset + length]

        explanation_chain = chain(
            set_waiting_explanation.si(message.chat_id, True),
            get_explanation_prompt.si(context, language, source_language),
            get_openai_response.s(explanation_content),
            persist_explanation_task.s(
                context, explanation_content, message_id, "explanation"
            ),
            create_explanation_message.s(offset, length),
        )

        response_data = explanation_chain.apply_async(
            link=set_waiting_explanation.si(message.chat_id, False)
        ).get()
        return JsonResponse(response_data)
    except Exception as e:
        return HttpResponse(
            "An error occurred while processing the request: " + str(e),
            status=status.HTTP_500_INTERNAL_SERVER_ERROR,
        )


@api_view(["GET"])
def get_translation(request):
    message_id = int(request.GET.get("message_id"))
    offset = int(request.GET.get("offset"))
    length = int(request.GET.get("length"))
    source_language = request.GET.get("source_language")

    if None in [message_id, offset, length, source_language]:
        return HttpResponse(
            "All required fields must be provided.", status=status.HTTP_400_BAD_REQUEST
        )

    message = get_object_or_404(Message, pk=message_id)
    if is_waiting_explanation(message.chat_id):
        return HttpResponse(
            "A translation is already in process. Try again after the current translation finishes.",
            status=status.HTTP_429_TOO_MANY_REQUESTS,
        )

    try:
        Chat.objects.filter(pk=message.chat_id).update(waiting_explanation=True)
        text = insert_string(message.content, "x|x ", offset)
        text = insert_string(text, " x|x", offset + length + 3)

        translation = translate_text(text=text, target_lang=source_language)

        if translation:
            Explanation.objects.create(
                type="translation",
                offset=offset,
                length=length,
                content=translation,
                message=message,
            )
            return JsonResponse(
                {
                    "type": "translation",
                    "offset": offset,
                    "length": length,
                    "content": translation,
                }
            )
        else:
            return HttpResponse(
                "An error occurred while processing the request.",
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )
    except Exception as e:
        return HttpResponse(
            "An error occurred while processing the request: " + str(e),
            status=status.HTTP_500_INTERNAL_SERVER_ERROR,
        )
    finally:
        Chat.objects.filter(pk=message.chat_id).update(waiting_explanation=False)


def insert_string(base_string, string_to_insert, index):
    return base_string[:index] + string_to_insert + base_string[index:]
