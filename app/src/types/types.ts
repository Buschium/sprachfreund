export interface IUser {
	id: number;
	firstName: string;
	lastName: string;
	token?: string;
}

export interface ISettings {
	darkMode: boolean;
}

export interface IConversation {
	id: number;
	messages: IMessage[];
	draftMessage: string;
	explanations: IExplanation[];
	waitingForAnswer: boolean;
	hasAnswerError: boolean;
}

export interface IMessage {
	type: 'message' | 'placeholder' | 'patch' | 'correction';
	id: number;
	content: string;
	date: string;
	sender: IUser;
	corrections: ICorrection[];
	explanations?: IExplanation[];
	hasCorrectionsError: boolean;
}

export interface ICorrection {
	body: string;
	offset: number;
	length: number;
	ruleId: string;
	ruleSubId?: number;
	category: string;
}

export interface IExplanation {
	type:
		| 'translation'
		| 'explanation'
		| 'translation-placeholder'
		| 'explanation-placeholder';
	message_id: number;
	offset: number;
	length: number;
	content: string;
}

export interface IEmoji {
	n: string[];
	u: string;
	r?: string;
	v?: string[];
}

export type Status =
	| 'idle'
	| 'loading'
	| 'step1'
	| 'step2'
	| 'step3'
	| 'step4'
	| 'success'
	| 'error';
