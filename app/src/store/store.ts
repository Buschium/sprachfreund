import { defineStore } from 'pinia';
import axios, { AxiosResponse } from 'axios';
import defaults from '@/store/defaults';
import { WebSocket, type ReconnectingWebSocket } from 'partysocket';
import i18n from '@/modules/i18n';
import type { Ref } from 'vue';
import type {
	IUser,
	ISettings,
	IConversation,
	IMessage,
	Status,
} from '@/types/types';

const MAX_RETRIES = 3;

export const useStore = defineStore('chat', {
	state: () => {
		// app status refs
		const status: Ref<Status> = ref('idle');
		const getStatus = computed(() => status);
		const connection: Ref<ReconnectingWebSocket | null> = ref(null);
		const connectionLost: Ref<boolean> = ref(false);
		const connectionRetries: Ref<number> = ref(0);
		const tokenPromise: Ref<Promise<AxiosResponse> | null> = ref(null);

		// message status refs
		const isWaitingForResponse = ref(false);
		const isWaitingForExplanation = ref(false);
		const isLoadingMessages = ref(false);

		// data refs
		const user: Ref<IUser | undefined> = ref(undefined);
		const settings: Ref<ISettings> = ref(
			JSON.parse(
				localStorage.getItem('settings') ?? defaults.defaultSettings
			)
		);
		const conversation: Ref<IConversation | undefined> = ref(undefined);
		const targetLanguage: Ref<string> = ref('');
		const sourceLanguage: Ref<string> = ref('');
		const targetLevel: Ref<string> = ref('');

		// ui refs
		const dir: Ref<'ltr' | 'rtl'> = ref('ltr');
		const showTimeoutModal: Ref<boolean> = ref(false);
		const messageTimeout: Ref<NodeJS.Timeout | null> = ref(null);
		const explanationTimeout: Ref<NodeJS.Timeout | null> = ref(null);
		const showChatCreationModal: Ref<boolean> = ref(false);
		const showMessageErrorModal: Ref<boolean> = ref(false);

		return {
			// status refs
			status,
			getStatus,
			connection,
			isWaitingForResponse,
			isWaitingForExplanation,
			connectionLost,
			connectionRetries,
			isLoadingMessages,
			tokenPromise,

			// data refs
			user,
			settings,
			conversation,
			targetLanguage,
			sourceLanguage,
			targetLevel,

			// ui refs
			dir,
			showTimeoutModal,
			messageTimeout,
			explanationTimeout,
			showChatCreationModal,
			showMessageErrorModal,
		};
	},
	actions: {
		setStatus(newStatus: Status) {
			this.status = newStatus;
		},

		async fetchChatHistory(chatId: string) {
			this.setStatus('loading');

			try {
				const response = await axios.get(
					`http://localhost:8000/api/chat/${chatId}/`
				);
				this.conversation = {
					...response.data,
					messages: [],
					explanations: [],
				};
				this.setLocale(
					response.data.language,
					response.data.source_language
				);
				if (response.data.waitingForAnswer) {
					this.setWaitingForResponse();
				}
				this.isWaitingForResponse = response.data.waitingForAnswer;
				this.isWaitingForExplanation =
					response.data.waitingForExplanation;
				this.setStatus('success');
			} catch (error) {
				this.setStatus('error');
				console.error('There was an error fetching the data:', error);
			}
		},

		async initializeWebSocket(chatId: string, status: Status) {
			if (this.connection) return;

			this.setStatus('loading');

			if (this.tokenPromise) {
				await this.tokenPromise;
			}

			await this.fetchChatHistory(chatId);

			this.connection = new WebSocket(
				`ws://localhost:8000/ws/chat/${chatId}/?token=${this.user?.token}`,
				[],
				{
					maxRetries: MAX_RETRIES,
				}
			);

			this.connection.onopen = event => {
				this.connectionLost = false;
				this.setStatus(status);
				console.log('WebSocket opened:', event);
			};

			this.connection.onmessage = event => {
				const data = JSON.parse(event.data);

				if (data.type === 'message_error') {
					console.error(`message_error from server: ${data.content}`);
					this.showMessageErrorModal = true;
					if (this.messageTimeout) {
						clearTimeout(this.messageTimeout);
						this.messageTimeout = null;
					}
					this.isWaitingForResponse = false;
					return;
				} else if (data.type === 'corrections_error') {
					console.error(
						`corrections_error from server: ${data.content}`
					);
					const messageIndex = this.conversation?.messages.findIndex(
						message => message.id === data.id
					);
					if (
						this.conversation &&
						messageIndex &&
						messageIndex >= 0
					) {
						this.conversation.messages[
							messageIndex
						].hasCorrectionsError = true;
					}
					return;
				} else if (data.type === 'answer_error') {
					console.error(`answer_error from server: ${data.content}`);
					if (this.conversation) {
						this.conversation.hasAnswerError = true;
					}
					if (this.messageTimeout) {
						clearTimeout(this.messageTimeout);
						this.messageTimeout = null;
					}
					this.isWaitingForResponse = false;
					this.showTimeoutModal = true;
					return;
				} else if (
					this.user &&
					data.sender.id !== this.user.id &&
					data.type === 'patch' &&
					this.messageTimeout
				) {
					clearTimeout(this.messageTimeout);
					this.messageTimeout = null;
					this.isWaitingForResponse = false;
				}
				this.addMessage(data);
			};

			this.connection.onclose = event => {
				console.log('WebSocket closed:', event);
				this.connection = null;
			};

			this.connection.onerror = error => {
				this.connectionLost = true;
				this.connectionRetries++;
				console.error('WebSocket Error:', error);
			};
		},

		sendMessage(message: IMessage) {
			if (!this.connection) return;
			this.connection.send(JSON.stringify(message));
			this.setWaitingForResponse();
		},

		sendRetryRequest(messageId: number) {
			if (!this.connection || !this.conversation) return;
			this.conversation.hasAnswerError = false;
			this.connection.send(
				JSON.stringify({
					type: 'retry',
					id: messageId,
				})
			);
			this.setWaitingForResponse();
		},

		addMessage(message: IMessage) {
			if (!this.conversation) {
				return;
			} else if (message.type === 'patch') {
				// Case 1: Replace placeholder message
				this.patchPlaceholderMessage(message);
			} else if (message.type === 'correction') {
				// Case 2: The server message contains corrections to the user message
				this.addMessageCorrections(message);
			} else if (
				!this.conversation?.messages.find(m => m.id === message.id)
			) {
				// Case 3: There is no placeholder message to be replaced
				if (message.explanations) {
					this.conversation.explanations =
						this.conversation.explanations.concat(
							message.explanations
						);
					delete message.explanations;
				}
				this.conversation?.messages.push(message);
			}
		},

		patchPlaceholderMessage(patchMessage: IMessage) {
			const placeholderIndex = this.conversation?.messages.findIndex(
				message => message.type === 'placeholder'
			);
			if (placeholderIndex !== undefined && this.conversation) {
				this.conversation.messages[placeholderIndex].type = 'message';
				this.conversation.messages[placeholderIndex].content =
					patchMessage.content;
				this.conversation.messages[placeholderIndex].date =
					patchMessage.date;
			}
		},

		addMessageCorrections(correctionsMessage: IMessage) {
			const messageIndex = this.conversation?.messages.findIndex(
				message => message.id === correctionsMessage.id
			);
			if (
				messageIndex !== undefined &&
				this.conversation &&
				correctionsMessage.content
			) {
				this.conversation.messages[messageIndex].corrections =
					JSON.parse(correctionsMessage.content);
			}
		},

		closeConnection() {
			if (this.connection) {
				this.connection.close();
				this.connection = null;
			}
		},

		isRetryingForConnection() {
			return this.connectionRetries < MAX_RETRIES;
		},

		setWaitingForResponse() {
			this.isWaitingForResponse = true;

			this.messageTimeout = setTimeout(() => {
				if (this.isWaitingForResponse) {
					this.isWaitingForResponse = false;
					this.showTimeoutModal = true;
				}
				if (this.conversation) {
					this.conversation.hasAnswerError = true;
				}
			}, 30000);
		},

		setWaitingForExplanation() {
			this.isWaitingForExplanation = true;

			this.explanationTimeout = setTimeout(() => {
				if (this.isWaitingForExplanation) {
					this.isWaitingForExplanation = false;
					this.showTimeoutModal = true;
				}
			}, 30000);
		},

		async fetchMessages(
			chatId: number,
			limit: number = 50,
			offset: number = 0
		) {
			try {
				const response = await axios.get(
					`http://localhost:8000/api/chat/${chatId}/messages/`,
					{
						params: {
							limit,
							offset,
						},
					}
				);

				if (
					response.status === 200 &&
					response.data &&
					this.conversation
				) {
					this.conversation.messages = [
						...response.data,
						...this.conversation.messages,
					];
				}
			} catch (error) {
				console.error('Error fetching messages:', error);
			} finally {
				this.isLoadingMessages = false;
			}
		},

		async fetchUserData(status: Status) {
			this.setStatus('loading');

			try {
				this.tokenPromise = axios.get(`http://localhost:8000/token/`, {
					withCredentials: true,
				});
				const response = await this.tokenPromise;
				this.user = {
					id: 1,
					firstName: 'Dummy',
					lastName: 'User',
					token: response.data.session_token,
				};
				console.log('Got token: ' + response.data.session_token);
				this.tokenPromise = null;
			} catch (error) {
				this.setStatus('error');
				console.error('There was an error fetching the data:', error);
			}

			this.setStatus(status);
		},

		async createChat(
			successStatus: Status,
			errorStatus: Status
		): Promise<IConversation | undefined> {
			this.setStatus('loading');
			try {
				const data = {
					language: this.targetLanguage,
					source_language: this.sourceLanguage,
					level: this.targetLevel,
				};

				const response = await axios.post(
					`http://localhost:8000/api/create_chat/`,
					data,
					{ withCredentials: true }
				);

				if (response.status === 201 && response.data) {
					this.setStatus(successStatus);
					return response.data;
				}
			} catch (error) {
				console.error('Error creating new chat:', error);
				console.log(this.showChatCreationModal);
				this.showChatCreationModal = true;
				console.log(this.showChatCreationModal);
				this.setStatus(errorStatus);
			}
		},

		async setLocale(target: string | null, source: string | null) {
			if (source) {
				i18n.global.locale.value = source;
				this.sourceLanguage = source;
				localStorage.setItem('user-locale', source);
				if (source === 'ar') {
					this.dir = 'rtl';
				} else {
					this.dir = 'ltr';
				}
			}
			if (target) {
				this.targetLanguage = target;
			}
		},

		async setDarkMode(darkMode: boolean) {
			this.settings.darkMode = darkMode;
			localStorage.setItem('settings', JSON.stringify(this.settings));
		},

		async getTranslation(
			message: IMessage,
			offset: number,
			length: number
		): Promise<boolean> {
			if (this.isWaitingForExplanation) return false;
			try {
				// Set waiting status and add placeholder
				this.isWaitingForExplanation = true;
				const explanationsLength = this.conversation?.explanations.push(
					{
						type: 'translation-placeholder',
						message_id: message.id,
						offset: offset,
						length: length,
						content: '',
					}
				);

				// Get translation from server
				const response = await axios.get(
					`http://localhost:8000/api/translation/`,
					{
						params: {
							message_id: message.id,
							offset,
							length,
							source_language: this.sourceLanguage,
						},
					}
				);
				if (
					response.status === 200 &&
					response.data &&
					this.conversation &&
					explanationsLength
				) {
					this.conversation.explanations[
						explanationsLength - 1
					].type = 'translation';
					this.conversation.explanations[
						explanationsLength - 1
					].content = response.data.content;
					return true;
				} else {
					return false;
				}
			} catch (error) {
				this.setStatus('error');
				console.error('There was an error fetching the data:', error);
				return false;
			} finally {
				this.isWaitingForExplanation = false;
			}
		},

		async getExplanation(
			message: IMessage,
			offset: number,
			length: number
		): Promise<boolean> {
			if (this.isWaitingForExplanation) return false;
			try {
				// Set waiting status and add placeholder
				this.isWaitingForExplanation = true;
				const explanationsLength = this.conversation?.explanations.push(
					{
						type: 'explanation-placeholder',
						message_id: message.id,
						offset: offset,
						length: length,
						content: '',
					}
				);

				const response = await axios.get(
					`http://localhost:8000/api/explanation/`,
					{
						params: {
							message_id: message.id,
							offset,
							length,
							language: this.targetLanguage,
							source_language: this.sourceLanguage,
						},
					}
				);
				if (
					response.status === 200 &&
					response.data &&
					this.conversation &&
					explanationsLength
				) {
					this.conversation.explanations[
						explanationsLength - 1
					].type = 'explanation';
					this.conversation.explanations[
						explanationsLength - 1
					].content = response.data.content;
					return true;
				} else {
					return false;
				}
			} catch (error) {
				this.setStatus('error');
				console.error('There was an error fetching the data:', error);
				return false;
			} finally {
				this.isWaitingForExplanation = false;
			}
		},
	},
});

export default useStore;
