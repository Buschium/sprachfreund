export const defaultSettings = JSON.stringify({
	darkMode: false,
});

export const user = {
	id: 1,
	firstName: 'Dawn',
	lastSeen: new Date(),
	lastName: 'Sabrina',
	email: 'sabrina@gmail.com',
	avatar: 'https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=387&q=80',
	token: 'fakeToken',
};

export const conversation = {
	id: 1,
	type: 'couple',
	draftMessage: '',
	messages: [
		{
			id: 1,
			content: 'Lorem ipsum dolor sit amet.',
			date: '3:00 pm',
			sender: {
				id: 6,
				email: 'user@gmail.com',
				firstName: 'Elijah',
				lastName: 'Sabrina',
				lastSeen: new Date(),
				avatar: 'https://images.unsplash.com/photo-1539571696357-5a69c17a67c6?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=387&q=80',
			},
		},
		{
			id: 2,
			content: 'Lorem ipsum dolor sit amet.',
			date: '4:00 pm',
			sender: {
				id: 1,
				firstName: 'Dawn',
				lastName: 'Sabrina',
				lastSeen: new Date(),
				email: 'sabrina@gmail.com',
				avatar: 'https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=387&q=80',
			},
		},
		{
			id: 3,
			content: 'Lorem ipsum dolor sit amet.',
			date: '3:00 pm',
			sender: {
				id: 6,
				email: 'user@gmail.com',
				firstName: 'Elijah',
				lastName: 'Sabrina',
				lastSeen: new Date(),
				avatar: 'https://images.unsplash.com/photo-1539571696357-5a69c17a67c6?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=387&q=80',
			},
		},
		{
			id: 4,
			content: 'Check this out https://github.com/',
			date: '4:00 pm',
			sender: {
				id: 6,
				email: 'user@gmail.com',
				firstName: 'Elijah',
				lastName: 'Sabrina',
				lastSeen: new Date(),
				avatar: 'https://images.unsplash.com/photo-1539571696357-5a69c17a67c6?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=387&q=80',
			},
		},
		{
			id: 5,
			content: 'Lorem ipsum dolor sit amet.',
			date: '5:00 pm',
			sender: {
				id: 1,
				firstName: 'Dawn',
				lastName: 'Sabrina',
				lastSeen: new Date(),
				email: 'sabrina@gmail.com',
				avatar: 'https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=387&q=80',
			},
		},
	],
};

export const notifications = [
	{
		flag: 'security',
		title: 'Recent Login',
		message: 'there was a recent login to you account from this device',
	},
	{
		flag: 'account-update',
		title: 'Password Reset',
		message: 'Your password has been restored successfully',
	},
	{
		flag: 'security',
		title: 'Recent Login',
		message: 'there was a recent login to you account from this device',
	},
];

const delay = (ms: number) => new Promise(resolve => setTimeout(resolve, ms));

export const fetchData = async () => {
	await delay(2000);

	return {
		data: {
			user: user,
			notifications: notifications,
			conversation: conversation,
		},
	};
};

export const updateAccount = async () => {
	await delay(4000);

	return {
		data: {
			detail: 'Your account has been updated successfully',
		},
	};
};

export default {
	defaultSettings,
	conversation,
	notifications,
	user,
} as const;
