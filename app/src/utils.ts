import type { IUser } from './types/types';
import ArabFlag from '@/assets/flags/ar.svg';
import GermanFlag from '@/assets/flags/de.svg';
import EnglishFlag from '@/assets/flags/en.svg';
import SpanishFlag from '@/assets/flags/es.svg';
import FrenchFlag from '@/assets/flags/fr.svg';
import RussianFlag from '@/assets/flags/ru.svg';
import TurkishFlag from '@/assets/flags/tr.svg';
import UkrainianFlag from '@/assets/flags/uk.svg';

/**
 * combine first name and last name of a contact.
 * @param contact
 * @returns A string the combines the first and last names.
 */
export const getFullName = (user: IUser) => {
	return user.firstName + ' ' + user.lastName;
};

/**
 * Convert unicode to native emoji
 *
 * @param unicode - emoji unicode
 */
export const unicodeToEmoji = (unicode: string) => {
	return unicode
		.split('-')
		.map(hex => parseInt(hex, 16))
		.map(hex => String.fromCodePoint(hex))
		.join('');
};

/**
 * Returns a debounced version of the function passed as parameter.
 * @param func The function to debounce
 * @param delay The debounce delay
 * @returns A function with the same signature as the parameter function, but debounced
 */
export function debounce<T extends (...args: any[]) => any>(
	func: T,
	delay: number
): T {
	let timer: NodeJS.Timeout | null = null;
	return function (...args: Parameters<T>) {
		if (timer) {
			clearTimeout(timer);
		}
		timer = setTimeout(() => {
			func(...args);
		}, delay);
	} as T;
}

/**
 * Function for getting the flag associated with a language code.
 * @param code The ISO 639-1 code for the language
 * @returns The imported flag
 */
export function getLanguageFlag(code: string) {
	switch (code) {
		case 'ar':
			return ArabFlag;
		case 'de':
			return GermanFlag;
		case 'en':
			return EnglishFlag;
		case 'es':
			return SpanishFlag;
		case 'fr':
			return FrenchFlag;
		case 'ru':
			return RussianFlag;
		case 'tr':
			return TurkishFlag;
		case 'uk':
			return UkrainianFlag;
	}
}
