#!/bin/bash

# Start Django server
cd api && python manage.py runserver &
DJANGO_PID=$!

# Start Vue.js frontend
cd app && pnpm run dev &
VUE_PID=$!

# Trap will handle the processes in case of a signal interrupt (Ctrl+C)
trap "kill $DJANGO_PID $VUE_PID" SIGINT

wait